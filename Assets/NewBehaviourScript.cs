﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NewBehaviourScript : MonoBehaviour
{
    public GameObject Dig;
    public GameObject Dog;
    public GameObject Dag;
    public Vector3 mousePos;
    public Animator animator;

    // Start is called before the first frame update
    void Start()
    {
        animator.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonUp(0))
        {
            mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Debug.Log(mousePos);
            if(mousePos.x <= -2.1f && mousePos.x >= -3.5f && mousePos.y <= -1.1f && mousePos.y >= -4.6f)
            {
                animator.Play("carOne");
            }
            if(mousePos.x <= 0.6f && mousePos.x >= -0.8f && mousePos.y <= -1.1f && mousePos.y >= -4.6f)
            {
                animator.Play("Car2");
            }
            if (mousePos.x <= 3.6f && mousePos.x >= 2.3f && mousePos.y <= -1.2f && mousePos.y >= -4.7f)
            {
                animator.Play("Car3");
            }
        }
    }
}
